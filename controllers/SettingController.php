<?php

namespace app\controllers;

use app\components\CController;
use app\models\Setting;
use app\models\SettingSearch;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends CController
{
    /**
     * Lists all Setting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingSearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single Setting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($setting_id)
    {
        Url::remember();
        return $this->render('view', [
            'model' => $this->findModel($setting_id),
        ]);
    }

    /**
     * Creates a new Setting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Setting;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(['view', 'setting_id' => $model->setting_id]);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing Setting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($setting_id)
    {
        $model = $this->findModel($setting_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['view', 'setting_id' => $model->setting_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Setting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($setting_id)
    {
        $this->findModel($setting_id)->delete();
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the Setting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Setting the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($setting_id)
    {
        if (($model = Setting::findOne($setting_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
