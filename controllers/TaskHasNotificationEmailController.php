<?php

namespace app\controllers;

use app\components\CController;
use app\models\TaskHasNotificationEmail;
use app\models\TaskHasNotificationEmailSearch;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * TaskHasNotificationEmailController implements the CRUD actions for TaskHasNotificationEmail model.
 */
class TaskHasNotificationEmailController extends CController
{
    /**
     * Lists all TaskHasNotificationEmail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskHasNotificationEmailSearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single TaskHasNotificationEmail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($task_has_notification_email_id)
    {
        Url::remember();
        return $this->render('view', [
            'model' => $this->findModel($task_has_notification_email_id),
        ]);
    }

    /**
     * Creates a new TaskHasNotificationEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskHasNotificationEmail;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing TaskHasNotificationEmail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($task_has_notification_email_id)
    {
        $model = $this->findModel($task_has_notification_email_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaskHasNotificationEmail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $task_id
     * @param $notification_email_id
     * @return mixed
     * @throws HttpException
     * @internal param int $id
     */
    public function actionDelete($task_id, $notification_email_id)
    {
        if (
            (
            $model = TaskHasNotificationEmail::find()
                ->where(
                    [
                        'task_has_notification_email_task_id'               => $task_id,
                        'task_has_notification_email_notification_email_id' => $notification_email_id]
                )->one()) !== null
        ) {
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }

        $model->delete();
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the TaskHasNotificationEmail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskHasNotificationEmail the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($task_has_notification_email_id)
    {
        if (($model = TaskHasNotificationEmail::findOne($task_has_notification_email_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
