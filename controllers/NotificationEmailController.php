<?php

namespace app\controllers;

use app\components\CController;
use app\models\NotificationEmail;
use app\models\NotificationEmailSearch;
use app\models\TaskHasNotificationEmail;
use app\models\TaskSearch;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * NotificationEmailController implements the CRUD actions for NotificationEmail model.
 */
class NotificationEmailController extends CController
{
    /**
     * Lists all NotificationEmail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationEmailSearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single NotificationEmail model.
     * @param integer $notification_email_id
     * @return mixed
     */
    public function actionView($notification_email_id)
    {
        Url::remember();
        $searchModel = new TaskSearch;
        $dataProvider = $searchModel->searchByEmail($_GET, $notification_email_id);
        return $this->render('view', [
            'model'        => $this->findModel($notification_email_id),
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new NotificationEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NotificationEmail;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(['view', 'notification_email_id' => $model->notification_email_id]);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing NotificationEmail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $notification_email_id
     * @return mixed
     */
    public function actionUpdate($notification_email_id)
    {
        $model = $this->findModel($notification_email_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['view', 'notification_email_id' => $model->notification_email_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NotificationEmail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $notification_email_id
     * @param null    $task_id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     */
    public function actionDelete($notification_email_id, $task_id = null)
    {
        if ($task_id) {
            if (
                (
                $model = TaskHasNotificationEmail::find()
                    ->where(
                        [
                            'task_has_notification_email_task_id'               => $task_id,
                            'task_has_notification_email_notification_email_id' => $notification_email_id]
                    )->one()) !== null
            ) {
            } else {
                throw new HttpException(404, 'The requested page does not exist.');
            }
            $model->delete();
        } else {
            $this->findModel($notification_email_id)->delete();
        }
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the NotificationEmail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $notification_email_id
     * @return NotificationEmail the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($notification_email_id)
    {
        if (($model = NotificationEmail::findOne($notification_email_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
