<?php

namespace app\controllers;

use app\components\CController;
use app\models\TaskCategory;
use app\models\TaskCategorySearch;
use app\models\TaskHasTaskCategory;
use app\models\TaskSearch;
use dosamigos\grid\ToggleAction;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * TaskCategoryController implements the CRUD actions for TaskCategory model.
 */
class TaskCategoryController extends CController
{
    public function actions()
    {
        return [
            'toggle' => [
                'class'      => ToggleAction::className(),
                'modelClass' => TaskCategory::className(),
                'onValue'    => 1,
                'offValue'   => 0
            ],
        ];
    }

    /**
     * Lists all TaskCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskCategorySearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single TaskCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($task_category_id)
    {
        Url::remember();
        $searchModelTask = new TaskSearch;
        $dataProviderTask = $searchModelTask->searchByTaskCategory($_GET, $task_category_id);
        return $this->render('view', [
            'model'            => $this->findModel($task_category_id),
            'dataProviderTask' => $dataProviderTask,
            'searchModelTask'  => $searchModelTask
        ]);
    }

    /**
     * Creates a new TaskCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskCategory;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(['view', 'task_category_id' => $model->task_category_id]);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing TaskCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($task_category_id)
    {
        $model = $this->findModel($task_category_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['view', 'task_category_id' => $model->task_category_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaskCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param      $task_category_id
     * @param null $task_id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @internal param int $id
     */
    public function actionDelete($task_category_id, $task_id = null)
    {
        if ($task_id) {
            if (
                (
                $model = TaskHasTaskCategory::find()
                    ->where(
                        [
                            'task_has_task_category_task_id'          => $task_id,
                            'task_has_task_category_task_category_id' => $task_category_id]
                    )->one()) !== null
            ) {
            } else {
                throw new HttpException(404, 'The requested page does not exist.');
            }
            $model->delete();
        } else {
            $this->findModel($task_category_id)->delete();
        }
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the TaskCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskCategory the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($task_category_id)
    {
        if (($model = TaskCategory::findOne($task_category_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
