<?php

namespace app\controllers;

use app\components\CController;
use app\models\NotificationEmailSearch;
use app\models\ParseResultSearch;
use app\models\Task;
use app\models\TaskCategorySearch;
use app\models\TaskHasNotificationEmail;
use app\models\TaskSearch;
use dosamigos\grid\ToggleAction;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends CController
{
    public function actions()
    {
        return [
            'toggle' => [
                'class'      => ToggleAction::className(),
                'modelClass' => Task::className(),
                'onValue'    => 1,
                'offValue'   => 0
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($task_id)
    {
        $searchModel = new NotificationEmailSearch();
        $dataProvider = $searchModel->searchByTask($_GET, $task_id);

        $searchModel = new TaskCategorySearch;
        $dataProviderCategory = $searchModel->searchByTask($_GET, $task_id);

        $searchModelResult = new ParseResultSearch;
        $dataProviderResult = $searchModelResult->searchByTask($_GET, $task_id);
        Url::remember();
        return $this->render('view', [
            'model'                => $this->findModel($task_id),
            'dataProviderEmail'    => $dataProvider,
            'dataProviderCategory' => $dataProviderCategory,
            'dataProviderResult'   => $dataProviderResult,
            'searchModelResult'    => $searchModelResult
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(['/task/view', 'task_id' => $model->primaryKey]);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($task_id)
    {
        $model = $this->findModel($task_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['/task/view', 'task_id' => $model->primaryKey]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param      $task_id
     * @param null $notification_email_id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @internal param int $id
     */
    public function actionDelete($task_id, $notification_email_id = null)
    {
        if ($notification_email_id) {
            if (
                (
                $model = TaskHasNotificationEmail::find()
                    ->where(
                        [
                            'task_has_notification_email_task_id'               => $task_id,
                            'task_has_notification_email_notification_email_id' => $notification_email_id]
                    )->one()) !== null
            ) {
            } else {
                throw new HttpException(404, 'The requested page does not exist.');
            }
            $model->delete();
        } else {
            $this->findModel($task_id)->delete();
        }

        return $this->redirect(Url::previous());
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($task_id)
    {
        if (($model = Task::findOne($task_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
