<?php

namespace app\controllers;

use app\components\CController;
use app\models\TaskHasTaskCategory;
use app\models\TaskHasTaskCategorySearch;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * TaskHasTaskCategoryController implements the CRUD actions for TaskHasTaskCategory model.
 */
class TaskHasTaskCategoryController extends CController
{
    /**
     * Lists all TaskHasTaskCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskHasTaskCategorySearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single TaskHasTaskCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($task_has_task_category_id)
    {
        Url::remember();
        return $this->render('view', [
            'model' => $this->findModel($task_has_task_category_id),
        ]);
    }

    /**
     * Creates a new TaskHasTaskCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskHasTaskCategory;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing TaskHasTaskCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($task_has_task_category_id)
    {
        $model = $this->findModel($task_has_task_category_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaskHasTaskCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($task_has_task_category_id)
    {
        $this->findModel($task_has_task_category_id)->delete();
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the TaskHasTaskCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskHasTaskCategory the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($task_has_task_category_id)
    {
        if (($model = TaskHasTaskCategory::findOne($task_has_task_category_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
