<?php

namespace app\controllers;

use app\components\CController;
use app\models\ParseResult;
use app\models\ParseResultSearch;
use yii\web\HttpException;
use yii\helpers\Url;

/**
 * ParseResultController implements the CRUD actions for ParseResult model.
 */
class ParseResultController extends CController
{
    /**
     * Lists all ParseResult models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParseResultSearch;
        $dataProvider = $searchModel->search($_GET);

        Url::remember();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single ParseResult model.
     * @param $parse_result_id
     * @return mixed
     * @throws HttpException
     * @internal param int $id
     */
    public function actionView($parse_result_id)
    {
        Url::remember();
        if (
            ($model = ParseResult::find()
                ->where(['parse_result_id' => $parse_result_id])
                ->with(['parseResultTask'])
                ->one()
            ) !== null) {
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ParseResult model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ParseResult;

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing ParseResult model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($parse_result_id)
    {
        $model = $this->findModel($parse_result_id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['view', 'parse_result_id' => $model->parse_result_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParseResult model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($parse_result_id)
    {
        $this->findModel($parse_result_id)->delete();
        return $this->redirect(Url::previous());
    }

    /**
     * Finds the ParseResult model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParseResult the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($parse_result_id)
    {
        if (($model = ParseResult::findOne($parse_result_id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
