<?php

return [
    'class'               => 'yii\db\Connection',
    'dsn'                 => 'mysql:host=localhost;dbname=torrent_parser',
    'username'            => 'root',
    'password'            => '3e4LaxYUC7',
    'charset'             => 'utf8',
    'schemaCacheDuration' => 3600 * 24, // cache DB schemas for 1 day
    'enableSchemaCache'   => true,
    'schemaCache'         => 'cache',
];
