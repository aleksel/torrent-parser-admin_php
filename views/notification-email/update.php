<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\NotificationEmail $model
 */

$this->title = Yii::t('app', 'Update Notification Email: {email}', ['email' => $model->notification_email_email]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notification Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'View Notification Email: {email}', ['email' => $model->notification_email_email]),
    'url'   => ['view', 'notification_email_id' => $model->notification_email_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-email-update">

    <p>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'),
            ['view', 'notification_email_id' => $model->notification_email_id], ['class' => 'btn btn-info']
        ) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
