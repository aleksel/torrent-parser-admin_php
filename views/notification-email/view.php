<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                 $this
 * @var app\models\NotificationEmail $model
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\TaskSearch       $searchModel
 */

$this->title = Yii::t('app', 'View Notification Email: {email}', ['email' => $model->notification_email_email]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notification Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="notification-email-view">

    <p class='pull-left'>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'),
            ['update', 'notification_email_id' => $model->notification_email_id],
            ['class' => 'btn btn-info']
        );
        ?>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
            ['delete', 'notification_email_id' => $model->notification_email_id],
            [
                'class'        => 'btn btn-danger',
                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                'data-method'  => 'post',
            ]
        );
        ?>
    </p>

    <div class='clearfix'></div>
    <br/>

    <?php $this->beginBlock('app\models\NotificationEmail'); ?>

    <?php echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            //'notification_email_id',
            'notification_email_email:email',
            'notification_email_create_time',
            'notification_email_update_time',
        ],
    ]); ?>

    <hr/>

    <?php $this->endBlock(); ?>

    <?php $this->beginBlock('TaskHasNotificationEmails'); ?>
    <p class='pull-right'>
        <?=
        \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Task Has Notification Email'),
            ['task-has-notification-email/create', 'TaskHasNotificationEmail' => ['task_has_notification_email_notification_email_id' => $model->notification_email_id]],
            ['class' => 'btn btn-success btn-xs']
        ) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'task_id',
            //'task_auth_data_id',
            'task_url:url',
            'task_name',
            'task_search_tag',
            'task_comparison_element:ntext',
            [
                'class' => \dosamigos\grid\ToggleColumn::className(),
                'attribute' => 'task_active',
                'onValue' => 1,
                'onLabel' => \Yii::t('app', 'Да'),
                'offLabel' => \Yii::t('app', 'Нет'),
                'contentOptions' => ['class' => 'text-center'],
                'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                'url' => ['/task/toggle'],
                'filter' => [
                    1 => 'Active',
                    0 => 'Not active'
                ]
            ],
            'task_create_time',
            'task_update_time',
            [
                'class'          => 'yii\grid\ActionColumn',
                'urlCreator'     => function ($action, $modelTask, $key, $index) use ($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = [$modelTask->primaryKey()[0] => (string)$key, 'notification_email_id' => $model->notification_email_id];
                    $params[0] = '/task/' . $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap' => 'nowrap']
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
    <div class='clearfix'></div>
    <?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [[
                'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ' . Yii::t('app', 'NotificationEmail'),
                'content' => $this->blocks['app\models\NotificationEmail'],
                'active'  => true,
            ], [
                'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> '
                    . Yii::t('app', 'Task Has Notification Emails') . '</small>',
                'content' => $this->blocks['TaskHasNotificationEmails'],
                'active'  => false,
            ],]
        ]
    );
    ?></div>
