<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View                       $this
 * @var yii\data\ActiveDataProvider        $dataProvider
 * @var app\models\NotificationEmailSearch $searchModel
 */

$this->title = Yii::t('app', 'New Notification Emails');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="notification-email-index">

    <div class="clearfix">
        <p class="pull-left">
            <?=
            Html::a(
                '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Notification Email'),
                ['create'],
                ['class' => 'btn btn-success']
            );
            ?>
        </p>
    </div>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel'  => $searchModel,
        'columns'      => [

            //'notification_email_id:email',
            'notification_email_email:email',
            'notification_email_create_time',
            'notification_email_update_time',
            [
                'class'          => 'yii\grid\ActionColumn',
                'urlCreator'     => function ($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap' => 'nowrap']
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
