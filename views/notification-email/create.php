<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\NotificationEmail $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Notification Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-email-create">

    <div class="clearfix"></div>

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
