<?php

use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var app\models\ParseResult $model
 */

$this->title = Yii::t('app', 'Update Parse Result: {result}', ['result' => $model->parse_result_id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parse Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'View Parse Result: {result}', ['result' => $model->parse_result_id]),
    'url'   => ['view', 'parse_result_id' => $model->parse_result_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="parse-result-update">

    <p>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'),
            ['view', 'parse_result_id' => $model->parse_result_id],
            ['class' => 'btn btn-info']
        );
        ?>
    </p>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
