<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View           $this
 * @var app\models\ParseResult $model
 */

$this->title = Yii::t('app', 'View Parse Result: {result}', ['result' => $model->parse_result_id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parse Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parse-result-view">

    <p class='pull-left'>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'),
            ['update', 'parse_result_id' => $model->parse_result_id],
            ['class' => 'btn btn-info']
        );
        ?>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
            ['delete', 'parse_result_id' => $model->parse_result_id],
            [
                'class'        => 'btn btn-danger',
                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                'data-method'  => 'post',
            ]
        );
        ?>
    </p>
    <div class='clearfix'></div>
    <?php $this->beginBlock('app\models\ParseResult'); ?>

    <?php echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'parse_result_id',
            [
                'attribute' => 'parse_result_task_id',
                'label'     => Yii::t('app', 'Task'),
                'format'    => 'html',
                'value'     => Html::a(
                    '<span class="label label-primary">' . $model->parseResultTask->task_name . '</span>',
                    ['/task/view', 'task_id' => $model->parseResultTask->task_id]
                )
            ],
            'parse_result_page_changed:boolean',
            'parse_result_raw_page:ntext',
            'parse_result_searched_element',
            'parse_result_create_time',
        ],
    ]); ?>

    <hr/>

    <?php $this->endBlock(); ?>
    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [[
                'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ' . Yii::t('app', 'ParseResult'),
                'content' => $this->blocks['app\models\ParseResult'],
                'active'  => true,
            ],]
        ]
    );
    ?></div>
