<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\ParseResult $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parse Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parse-result-create">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
