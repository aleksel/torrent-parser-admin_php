<?php

use app\models\Task;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View           $this
 * @var app\models\ParseResult $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="parse-result-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>

            <?= $form->field($model, 'parse_result_task_id')
                ->dropDownList(ArrayHelper::map(Task::find()->all(), 'task_id', 'task_name')) ?>
            <?= $form->field($model, 'parse_result_page_changed')
                ->dropDownList([0 => Yii::t('app', 'Нет'), 1 => Yii::t('app', 'Да')]) ?>
            <?= $form->field($model, 'parse_result_raw_page')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'parse_result_searched_element')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?=
        \yii\bootstrap\Tabs::widget(
            [
                'encodeLabels' => false,
                'items'        => [[
                    'label'   => 'ParseResult',
                    'content' => $this->blocks['main'],
                    'active'  => true,
                ],]
            ]
        );
        ?>
        <hr/>

        <?=
        Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> '
            . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']
        ); ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
