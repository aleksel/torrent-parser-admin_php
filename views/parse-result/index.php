<?php

use app\models\Task;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View                 $this
 * @var yii\data\ActiveDataProvider  $dataProvider
 * @var app\models\ParseResultSearch $searchModel
 */

$this->title = Yii::t('app', 'Parse Results');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="parse-result-index">

    <div class="clearfix">
    </div>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'parse_result_id',
            [
                'attribute' => 'parse_result_task_id',
                'format'    => 'html',
                'filter'    => ArrayHelper::map(Task::find()->orderBy('task_name')->all(), 'task_id', 'task_name'),
                'value'     => function ($model) {
                    /** @var \app\models\Task $model */
                    return isset($model->parseResultTask) ? Html::a(
                        '<span class="label label-primary">' . $model->parseResultTask->task_name . '</span>',
                        ['/task/view', 'task_id' => $model->parseResultTask->task_id]
                    ) : null;
                }
            ],
            [
                'attribute' => 'parse_result_page_changed',
                'format'    => 'boolean',
                'filter'    => [1 => Yii::t('app', 'Да'), 0 => Yii::t('app', 'Нет')]
            ],
            'parse_result_raw_page:ntext',
            'parse_result_searched_element',
            'parse_result_create_time',
            [
                'class'          => 'yii\grid\ActionColumn',
                'urlCreator'     => function ($action, $model, $key) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    /** @var \app\models\ParseResult $model */
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap' => 'nowrap']
            ],
        ],
    ]); ?>

</div>
