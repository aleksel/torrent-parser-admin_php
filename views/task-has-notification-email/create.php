<?php

/**
* @var yii\web\View $this
* @var app\models\TaskHasNotificationEmail $model
*/

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Has Notification Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-has-notification-email-create">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
