<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var app\models\TaskHasNotificationEmail $model
*/

$this->title = 'Task Has Notification Email View ' . $model->task_has_notification_email_id . '';
$this->params['breadcrumbs'][] = ['label' => 'Task Has Notification Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->task_has_notification_email_id, 'url' => ['view', 'task_has_notification_email_id' => $model->task_has_notification_email_id]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="task-has-notification-email-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'task_has_notification_email_id' => $model->task_has_notification_email_id],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Task Has Notification Email', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->task_has_notification_email_id ?>    </h3>


    <?php $this->beginBlock('app\models\TaskHasNotificationEmail'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'task_has_notification_email_id:email',
			'task_has_notification_email_task_id:email',
			'task_has_notification_email_notification_email_id:email',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'task_has_notification_email_id' => $model->task_has_notification_email_id],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> TaskHasNotificationEmail',
    'content' => $this->blocks['app\models\TaskHasNotificationEmail'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
