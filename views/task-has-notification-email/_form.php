<?php

use app\models\NotificationEmail;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View                        $this
 * @var app\models\TaskHasNotificationEmail $model
 * @var yii\widgets\ActiveForm              $form
 */
?>

<div class="task-has-notification-email-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>

            <?=
            $form->field($model, 'task_has_notification_email_task_id')
                ->dropDownList(ArrayHelper::map(\app\models\Task::find()->all(), 'task_id', 'task_name')) ?>
            <?=
            $form->field($model, 'task_has_notification_email_notification_email_id')
                ->dropDownList(
                    ArrayHelper::map(
                        NotificationEmail::find()->all(),
                        'notification_email_id',
                        'notification_email_email'
                    )
                ) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?=
        \yii\bootstrap\Tabs::widget(
            [
                'encodeLabels' => false,
                'items'        => [[
                    'label'   => Yii::t('app', 'TaskHasNotificationEmail'),
                    'content' => $this->blocks['main'],
                    'active'  => true,
                ],]
            ]
        );
        ?>
        <hr/>

        <?=
        Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> '
            . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']
        ) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
