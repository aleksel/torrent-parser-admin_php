<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TaskHasNotificationEmail $model
 */

$this->title = 'Task Has Notification Email Update ' . $model->task_has_notification_email_id . '';
$this->params['breadcrumbs'][] = ['label' => 'Task Has Notification Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->task_has_notification_email_id, 'url' => ['view', 'task_has_notification_email_id' => $model->task_has_notification_email_id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="task-has-notification-email-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'task_has_notification_email_id' => $model->task_has_notification_email_id], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
