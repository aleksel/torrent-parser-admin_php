<?php

use yii\bootstrap\Nav;

/** @var \app\models\User $user */

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= ucfirst($user->username); ?></p>

                <?=
                \yii\helpers\Html::a(
                    '<i class="fa fa-circle text-success"></i> Online</a>',
                    ['/user/view', 'id' => $user->id]
                ); ?>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <?=
        Nav::widget(
            [
                'encodeLabels' => false,
                'options'      => ['class' => 'sidebar-menu'],
                'items'        => [
                    '<li class="header">Menu Yii2</li>',
                    ['label' => '<i class="fa fa-file-code-o"></i><span>Gii</span>', 'url' => ['/gii']],
                    ['label' => '<i class="fa fa-dashboard"></i><span>Debug</span>', 'url' => ['/debug']],
                    ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                    [
                        'label' => '<span>Пользователи</span>',
                        'active' => Yii::$app->controller->id === 'user',
                        'items'        => [
                            '<li class="header">Управление пользователями</li>',
                            ['label' => '<span>Список пользователей</span>', 'url' => ['/user']],
                            ['label' => '<span>Создать пользователя</span>', 'url' => ['/user/create']],
                        ],
                    ],
                    [
                        'label' => '<span>Задачи</span>',
                        'active' => Yii::$app->controller->id === 'task'
                            || Yii::$app->controller->id === 'parse-result'
                            || Yii::$app->controller->id === 'task-category',
                        'items'        => [
                            '<li class="header">Управление задачами</li>',
                            ['label' => '<span>Список задач</span>', 'url' => ['/task']],
                            ['label' => '<span>Создать задачу</span>', 'url' => ['/task/create']],
                            ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                            ['label' => '<span>Список категорий</span>', 'url' => ['/task-category']],
                            ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                            ['label' => '<span>Список результатов</span>', 'url' => ['/parse-result']],
                        ],
                    ],
                    [
                        'label' => '<span>E-mail</span>',
                        'active' => Yii::$app->controller->id === 'notification-email',
                        'items'        => [
                            '<li class="header">Управление E-mail</li>',
                            ['label' => '<span>Список e-mail</span>', 'url' => ['/notification-email']],
                            ['label' => '<span>Создать e-mail</span>', 'url' => ['/notification-email/create']],
                        ],
                    ],
                    ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                    [
                        'label' => '<span>Настройки</span>',
                        'active' => Yii::$app->controller->id === 'setting',
                        'items'        => [
                            '<li class="header">Управление настройками</li>',
                            ['label' => '<span>Список настроек</span>', 'url' => ['/setting']],
                            ['label' => '<span>Создать настройку</span>', 'url' => ['/setting/create']],
                        ],
                    ],
                    ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                    [
                        'label'   => '<i class="glyphicon glyphicon-lock"></i><span>Вход</span>', //for basic
                        'url'     => ['/site/login'],
                        'visible' => Yii::$app->user->isGuest
                    ],
                    ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                    [
                        'label'   => '<i class="glyphicon glyphicon-lock"></i><span>Выход</span>', //for basic
                        'url'     => ['/site/logout'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                ],
            ]
        );
        ?>

    </section>

</aside>
