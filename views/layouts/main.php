<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
if (Yii::$app->controller->action->id === 'login') {
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    app\assets\AppAsset::register($this);
    app\assets\AdminLteAsset::register($this);

    $user = \app\models\User::findOne(Yii::$app->user->id);
    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/bower/admin-lte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?=
        $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset, 'user' => $user]
        ) ?>

        <?=
        $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset, 'user' => $user]
        )
        ?>

        <?=
        $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php
}
