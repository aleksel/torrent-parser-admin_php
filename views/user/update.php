<?php

use yii\helpers\Html;

/**
 * @var yii\web\View    $this
 * @var app\models\User $model
 */

$this->title = Yii::t('app', 'Update user: {user}', ['user' => $model->username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'View user: {user}', ['user' => $model->username]),
    'url'   => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update">

    <p>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'),
            ['view', 'id' => $model->id],
            ['class' => 'btn btn-info']
        );
        ?>
    </p>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
