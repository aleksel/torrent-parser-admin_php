<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View    $this
 * @var app\models\User $model
 */

$this->title = Yii::t('app', 'View user: {user}', ['user' => $model->username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p class='pull-left'>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'),
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-info']
        );
        ?>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
            ['delete', 'id' => $model->id],
            [
                'class'        => 'btn btn-danger',
                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                'data-method'  => 'post',
            ]
        );
        ?>
    </p>

    <div class='clearfix'></div>
    <br/>

    <?php $this->beginBlock('app\models\User'); ?>

    <?php echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            //'id',
            'username',
            //'auth_key',
            'password_hash',
            //'password_reset_token',
            'email:email',
            //'status',
            [
                'attribute' => 'created_at',
                'value'     => $model->created_at ? date('Y-m-d H:i:s', $model->created_at) : null
            ],
            [
                'attribute' => 'updated_at',
                'value'     => $model->updated_at ? date('Y-m-d H:i:s', $model->updated_at) : null
            ],
        ],
    ]); ?>

    <hr/>

    <?php $this->endBlock(); ?>

    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [[
                'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ' . Yii::t('app', 'User'),
                'content' => $this->blocks['app\models\User'],
                'active'  => true,
            ],]
        ]
    );
    ?></div>
