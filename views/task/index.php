<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\TaskSearch       $searchModel
 */

$this->title = Yii::t('app', 'Tasks');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="task-index">

    <?php //     echo $this->render('_search', ['model' =>$searchModel]);
    ?>

    <div class="clearfix">
        <p class="pull-left">
            <?=
            Html::a(
                '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Task'),
                ['create'],
                ['class' => 'btn btn-success']
            ) ?>
        </p>
    </div>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'task_id',
            [
                'attribute' => 'category',
                'label'     => Yii::t('app', 'Category'),
                'filter'    => \yii\helpers\ArrayHelper::map(\app\models\TaskCategory::find()->all(), 'task_category_id', 'task_category_name'),
                'format'    => 'html',
                'value'     => function ($model) {
                    $result = '';
                    /** @var \app\models\Task $model */
                    if ($model->taskHasTaskCategories) {
                        /** @var \app\models\TaskHasTaskCategory $parent */
                        foreach ($model->taskHasTaskCategories as $taskCategory) {
                            if (isset($taskCategory->taskHasTaskCategoryTaskCategory->task_category_name)) {
                                $result .= '<span class="label label-primary">'
                                    . Html::a(
                                        $taskCategory->taskHasTaskCategoryTaskCategory->task_category_name,
                                        ['/task-category/view', 'task_category_id' => $taskCategory->taskHasTaskCategoryTaskCategory->task_category_id],
                                        ['style' => 'color: white;']
                                    )
                                    . '</span> ';
                            }
                        }
                    }

                    return $result;
                },
            ],
            //'task_auth_data_id',
            'task_url:url',
            'task_name',
            'task_search_tag',
            'task_comparison_element:ntext',
            [
                'class'          => \dosamigos\grid\ToggleColumn::className(),
                'attribute'      => 'task_active',
                'onValue'        => 1,
                'onLabel'        => \Yii::t('app', 'Да'),
                'offLabel'       => \Yii::t('app', 'Нет'),
                'contentOptions' => ['class' => 'text-center'],
                'afterToggle'    => 'function(r, data){if(r){console.log("done", data)};}',
                'url'            => ['/task/toggle'],
                'filter'         => [
                    1 => 'Active',
                    0 => 'Not active'
                ]
            ],
            'task_create_time',
            'task_update_time',
            [
                'class'          => 'yii\grid\ActionColumn',
                'urlCreator'     => function ($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap' => 'nowrap']
            ],
        ],
    ]);
    ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
