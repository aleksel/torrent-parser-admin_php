<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View           $this
 * @var app\models\Task        $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>

            <?= $form->field($model, 'task_active')->dropDownList([1 => Yii::t('app', 'Активно'), 2 => Yii::t('app', 'Не активно')]) ?>
            <?= $form->field($model, 'task_url')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'task_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'task_comparison_element')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'task_search_tag')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?=
        \yii\bootstrap\Tabs::widget(
            [
                'encodeLabels' => false,
                'items'        => [
                    [
                        'label'   => Yii::t('app', 'Task'),
                        'content' => $this->blocks['main'],
                        'active'  => true,
                    ],
                ]
            ]
        );
        ?>
        <hr/>

        <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> '
            . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']
        ); ?>
        <?php ActiveForm::end(); ?>

    </div>

</div>
