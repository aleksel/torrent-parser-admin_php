<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                  $this
 * @var app\models\Task               $model
 * @var yii\data\ActiveDataProvider   $dataProviderEmail
 * @var yii\data\ActiveDataProvider   $dataProviderCategory
 * @var yii\data\ActiveDataProvider   $dataProviderResult
 * @var \app\models\ParseResultSearch $searchModelResult
 */

$this->title = Yii::t('app', 'View Task: {task}', ['task' => $model->task_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <p class='pull-left'>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'),
            ['update', 'task_id' => $model->task_id],
            ['class' => 'btn btn-info']
        );
        ?>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
            ['delete', 'task_id' => $model->task_id],
            [
                'class'        => 'btn btn-danger',
                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                'data-method'  => 'post',
            ]
        );
        ?>
    </p>

    <div class='clearfix'></div>

    <?php $this->beginBlock('app\models\Task'); ?>

    <?php
    $categories = '';
    /** @var \app\models\Task $model */
    if ($model->taskHasTaskCategories) {
        /** @var \app\models\TaskHasTaskCategory $parent */
        foreach ($model->taskHasTaskCategories as $taskCategory) {
            if (isset($taskCategory->taskHasTaskCategoryTaskCategory->task_category_name)) {
                $categories .= '<span class="label label-primary">'
                    . Html::a(
                        $taskCategory->taskHasTaskCategoryTaskCategory->task_category_name,
                        ['/task-category/view', 'task_category_id' => $taskCategory->taskHasTaskCategoryTaskCategory->task_category_id],
                        ['style' => 'color: white;']
                    )
                    . '</span> ';
            }
        }
    }

    $emails = '';
    /** @var \app\models\Task $model */
    if ($model->taskHasNotificationEmails) {
        /** @var \app\models\TaskHasNotificationEmail $parent */
        foreach ($model->taskHasNotificationEmails as $taskEmail) {
            if (isset($taskEmail->taskHasNotificationEmailNotificationEmail->notification_email_email)) {
                $emails .= '<span class="label label-primary">'
                    . Html::a(
                        $taskEmail->taskHasNotificationEmailNotificationEmail->notification_email_email,
                        ['/notification-email/view', 'notification_email_id' => $taskEmail->taskHasNotificationEmailNotificationEmail->notification_email_id],
                        ['style' => 'color: white;']
                    )
                    . '</span> ';
            }
        }
    }

    echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'task_id',
            //'task_auth_data_id',
            'task_url:url',
            'task_name',
            'task_search_tag',
            'task_comparison_element:ntext',
            'task_active:boolean',
            'task_create_time',
            'task_update_time',
            [
                'attribute' => 'category',
                'label'     => Yii::t('app', 'Category'),
                'format'    => 'html',
                'value'     => $categories,
            ],
            [
                'attribute' => 'emails',
                'label'     => Yii::t('app', 'Notification Emails'),
                'format'    => 'html',
                'value'     => $emails,
            ],
        ],
    ]); ?>

    <hr/>

    <?php $this->endBlock(); ?>

    <?php $this->beginBlock('ParseResults'); ?>
    <p class='pull-right'>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php echo GridView::widget([
            'dataProvider' => $dataProviderResult,
            'filterModel'  => $searchModelResult,
            'columns'      => [

                'parse_result_id',
                //'parse_result_task_id',
                [
                    'attribute' => 'parse_result_page_changed',
                    'format'    => 'boolean',
                    'filter'    => [1 => Yii::t('app', 'Да'), 0 => Yii::t('app', 'Нет')]
                ],
                'parse_result_raw_page:ntext',
                'parse_result_searched_element',
                'parse_result_create_time',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'urlCreator'     => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = '/parse-result/' . $action;
                        return \yii\helpers\Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </p>

    <div class='clearfix'></div>
    <?php $this->endBlock() ?>


    <?php $this->beginBlock('TaskHasNotificationEmails'); ?>
    <p class='pull-right'>
        <?=
        \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Task Has Notification Email'),
            ['task-has-notification-email/create', 'TaskHasNotificationEmail' => ['task_has_notification_email_task_id' => $model->task_id]],
            ['class' => 'btn btn-success btn-xs']
        ) ?>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php echo GridView::widget([
            'dataProvider' => $dataProviderEmail,
            //'filterModel'  => $searchModel,
            'columns'      => [

                //'notification_email_id:email',
                'notification_email_email:email',
                'notification_email_create_time',
                'notification_email_update_time',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'urlCreator'     => function ($action, $modelNotification, $key, $index) use ($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = [$modelNotification->primaryKey()[0] => (string)$key, 'task_id' => $model->task_id];
                        $params[0] = '/notification-email/' . $action;
                        return \yii\helpers\Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </p>

    <div class='clearfix'></div>
    <?php $this->endBlock() ?>


    <?php $this->beginBlock('TaskHasTaskCategories'); ?>
    <p class='pull-right'>
        <?=
        \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Task Has Task Category'),
            ['task-has-task-category/create', 'TaskHasTaskCategory' => ['task_has_task_category_task_id' => $model->task_id]],
            ['class' => 'btn btn-success btn-xs']
        ) ?>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php echo GridView::widget([
            'dataProvider' => $dataProviderCategory,
            //'filterModel'  => $searchModel,
            'columns'      => [

                'task_category_id',
                'task_category_name',
                [
                    'class'          => \dosamigos\grid\ToggleColumn::className(),
                    'attribute'      => 'task_category_active',
                    'onValue'        => 1,
                    'onLabel'        => \Yii::t('app', 'Да'),
                    'offLabel'       => \Yii::t('app', 'Нет'),
                    'contentOptions' => ['class' => 'text-center'],
                    'afterToggle'    => 'function(r, data){if(r){console.log("done", data)};}',
                    'url'            => ['/task-category/toggle'],
                    'filter'         => [
                        1 => 'Active',
                        0 => 'Not active'
                    ]
                ],
                'task_category_create_time',
                'task_category_update_time',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'urlCreator'     => function ($action, $modelCategory, $key, $index) use ($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = [$modelCategory->primaryKey()[0] => (string)$key, 'task_id' => $model->task_id];
                        $params[0] = '/task-category/' . $action;
                        return \yii\helpers\Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </p>

    <div class='clearfix'></div>
    <?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [[
                'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ' . Yii::t('app', 'Task'),
                'content' => $this->blocks['app\models\Task'],
                'active'  => true,
            ], [
                'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> </small>' . Yii::t('app', 'Parse Results'),
                'content' => $this->blocks['ParseResults'],
                'active'  => false,
            ], [
                'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> </small>' . Yii::t('app', 'Task Has Notification Emails'),
                'content' => $this->blocks['TaskHasNotificationEmails'],
                'active'  => false,
            ], [
                'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> </small>' . Yii::t('app', 'Task Has Task Categories'),
                'content' => $this->blocks['TaskHasTaskCategories'],
                'active'  => false,
            ],]
        ]
    );
    ?></div>
