<?php

use yii\helpers\Html;

/**
 * @var yii\web\View    $this
 * @var app\models\Task $model
 */

$this->title = Yii::t('app', 'Update Task: {task}', ['task' => $model->task_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'View Task: {task}', ['task' => $model->task_name]),
    'url'   => ['view', 'id' => $model->task_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-update">

    <p>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'),
            ['view', 'task_id' => $model->task_id],
            ['class' => 'btn btn-info']
        );
        ?>
    </p>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
