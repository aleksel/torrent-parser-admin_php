<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var app\models\TaskHasTaskCategory $model
 * @var yii\widgets\ActiveForm         $form
 */
?>

<div class="task-has-task-category-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            <?= $form->field($model, 'task_has_task_category_task_id')
                ->dropDownList(ArrayHelper::map(\app\models\Task::find()->all(), 'task_id', 'task_name')) ?>
            <?=
            $form->field($model, 'task_has_task_category_task_category_id')
                ->dropDownList(
                    ArrayHelper::map(
                        \app\models\TaskCategory::find()->all(),
                        'task_category_id',
                        'task_category_name'
                    )
                ) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?=
        \yii\bootstrap\Tabs::widget(
            [
                'encodeLabels' => false,
                'items'        => [[
                    'label'   => Yii::t('app', 'TaskHasTaskCategory'),
                    'content' => $this->blocks['main'],
                    'active'  => true,
                ],]
            ]
        );
        ?>
        <hr/>

        <?=
        Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> '
            . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']
        ) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
