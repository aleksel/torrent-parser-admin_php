<?php

/**
 * @var yii\web\View                   $this
 * @var app\models\TaskHasTaskCategory $model
 */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Has Task Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-has-task-category-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
