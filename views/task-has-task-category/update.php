<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\TaskHasTaskCategory $model
 */

$this->title = 'Task Has Task Category Update ' . $model->task_has_task_category_id . '';
$this->params['breadcrumbs'][] = ['label' => 'Task Has Task Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->task_has_task_category_id, 'url' => ['view', 'task_has_task_category_id' => $model->task_has_task_category_id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="task-has-task-category-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'task_has_task_category_id' => $model->task_has_task_category_id], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
