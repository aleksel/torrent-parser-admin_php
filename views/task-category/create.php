<?php

/**
 * @var yii\web\View            $this
 * @var app\models\TaskCategory $model
 */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-category-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
