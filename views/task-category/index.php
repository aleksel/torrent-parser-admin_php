<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View                  $this
 * @var yii\data\ActiveDataProvider   $dataProvider
 * @var app\models\TaskCategorySearch $searchModel
 */

$this->title = Yii::t('app', 'Task Categories');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="task-category-index">

    <div class="clearfix">
        <p class="pull-left">
            <?=
            Html::a(
                '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Task Category'),
                ['create'],
                ['class' => 'btn btn-success']
            )
            ?>
        </p>
    </div>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'task_category_id',
            'task_category_name',
            [
                'class'          => \dosamigos\grid\ToggleColumn::className(),
                'attribute'      => 'task_category_active',
                'onValue'        => 1,
                'onLabel'        => \Yii::t('app', 'Да'),
                'offLabel'       => \Yii::t('app', 'Нет'),
                'contentOptions' => ['class' => 'text-center'],
                'afterToggle'    => 'function(r, data){if(r){console.log("done", data)};}',
                'url'            => ['/task-category/toggle'],
                'filter'         => [
                    1 => 'Active',
                    0 => 'Not active'
                ]
            ],
            'task_category_create_time',
            'task_category_update_time',
            [
                'class'          => 'yii\grid\ActionColumn',
                'urlCreator'     => function ($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap' => 'nowrap']
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
