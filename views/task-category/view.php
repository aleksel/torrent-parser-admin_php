<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                 $this
 * @var app\models\TaskCategory      $model
 * @var \yii\data\ActiveDataProvider $dataProviderTask
 * @var app\models\TaskSearch        $searchModelTask
 */

$this->title = Yii::t('app', 'View Task Category: {category}', ['category' => $model->task_category_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-category-view">

    <p class='pull-left'>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'),
            ['update', 'task_category_id' => $model->task_category_id],
            ['class' => 'btn btn-info']
        );
        ?>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
            ['delete', 'task_category_id' => $model->task_category_id],
            [
                'class'        => 'btn btn-danger',
                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                'data-method'  => 'post',
            ]
        );
        ?>
    </p>

    <div class='clearfix'></div>
    <br/>
    <?php $this->beginBlock('app\models\TaskCategory'); ?>

    <?php echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'task_category_id',
            'task_category_name',
            'task_category_active:boolean',
            'task_category_create_time',
            'task_category_update_time',
        ],
    ]); ?>

    <hr/>

    <?php $this->endBlock(); ?>

    <?php $this->beginBlock('TaskHasTaskCategories'); ?>
    <p class='pull-right'>
        <?=
        \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New Task Has Task Category'),
            ['task-has-task-category/create', 'TaskHasTaskCategory' => ['task_has_task_category_task_category_id' => $model->task_category_id]],
            ['class' => 'btn btn-success btn-xs']
        ) ?>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php echo GridView::widget([
            'dataProvider' => $dataProviderTask,
            'filterModel'  => $searchModelTask,
            'columns'      => [

                'task_id',
                //'task_auth_data_id',
                'task_url:url',
                'task_name',
                'task_search_tag',
                'task_comparison_element:ntext',
                [
                    'class'          => \dosamigos\grid\ToggleColumn::className(),
                    'attribute'      => 'task_active',
                    'onValue'        => 1,
                    'onLabel'        => \Yii::t('app', 'Да'),
                    'offLabel'       => \Yii::t('app', 'Нет'),
                    'contentOptions' => ['class' => 'text-center'],
                    'afterToggle'    => 'function(r, data){if(r){console.log("done", data)};}',
                    'url'            => ['/task/toggle'],
                    'filter'         => [
                        1 => 'Active',
                        0 => 'Not active'
                    ]
                ],
                'task_create_time',
                'task_update_time',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'urlCreator'     => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = '/task/' . $action;
                        return \yii\helpers\Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]);
        ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </p>

    <div class='clearfix'></div>
    <?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [[
                'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ' . Yii::t('app', 'TaskCategory'),
                'content' => $this->blocks['app\models\TaskCategory'],
                'active'  => true,
            ], [
                'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> ' . Yii::t('app', 'Task Has Task Categories') . '</small>',
                'content' => $this->blocks['TaskHasTaskCategories'],
                'active'  => false,
            ],]
        ]
    );
    ?></div>
