<?php

use yii\helpers\Html;

/**
 * @var yii\web\View            $this
 * @var app\models\TaskCategory $model
 */

$this->title = Yii::t('app', 'Update Task Category: {category}', ['category' => $model->task_category_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'View Task Category: {category}', ['category' => $model->task_category_name]),
    'url'   => ['view', 'task_category_id' => $model->task_category_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-category-update">

    <p>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'),
            ['view', 'task_category_id' => $model->task_category_id],
            ['class' => 'btn btn-info']
        );
        ?>
    </p>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
