<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View            $this
 * @var app\models\TaskCategory $model
 * @var yii\widgets\ActiveForm  $form
 */
?>

<div class="task-category-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>

            <?= $form->field($model, 'task_category_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'task_category_active')->dropDownList([1 => Yii::t('app', 'Активно'), 2 => Yii::t('app', 'Не активно')]) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?=
        \yii\bootstrap\Tabs::widget(
            [
                'encodeLabels' => false,
                'items'        => [[
                    'label'   => 'TaskCategory',
                    'content' => $this->blocks['main'],
                    'active'  => true,
                ],]
            ]
        );
        ?>
        <hr/>

        <?=
        Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> '
            . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']
        ); ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
