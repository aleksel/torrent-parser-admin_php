<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View       $this
 * @var app\models\Setting $model
 */

$this->title = Yii::t('app', 'View Setting: {setting}', ['setting' => $model->setting_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-view">

    <p class='pull-left'>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'),
            ['update', 'setting_id' => $model->setting_id],
            ['class' => 'btn btn-info']
        );
        ?>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
            ['delete', 'setting_id' => $model->setting_id],
            [
                'class'        => 'btn btn-danger',
                'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                'data-method'  => 'post',
            ]
        );
        ?>
    </p>

    <div class='clearfix'></div>
    <br/>
    <?php $this->beginBlock('app\models\Setting'); ?>

    <?php echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'setting_id',
            'setting_name',
            'setting_system_name',
            'setting_value:ntext',
            'setting_type',
            'setting_create_time',
            'setting_update_time',
        ],
    ]); ?>

    <hr/>

    <?php $this->endBlock(); ?>
    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [[
                'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Setting',
                'content' => $this->blocks['app\models\Setting'],
                'active'  => true,
            ],]
        ]
    );
    ?></div>
