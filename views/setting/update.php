<?php

use yii\helpers\Html;

/**
 * @var yii\web\View       $this
 * @var app\models\Setting $model
 */

$this->title = Yii::t('app', 'Update Setting: {setting}', ['setting' => $model->setting_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'View Setting: {setting}', ['setting' => $model->setting_name]),
    'url'   => ['view', 'setting_id' => $model->setting_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-update">

    <p>
        <?=
        Html::a(
            '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'),
            ['view', 'setting_id' => $model->setting_id],
            ['class' => 'btn btn-info']
        );
        ?>
    </p>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
