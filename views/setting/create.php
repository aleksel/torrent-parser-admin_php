<?php

/**
 * @var yii\web\View       $this
 * @var app\models\Setting $model
 */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
