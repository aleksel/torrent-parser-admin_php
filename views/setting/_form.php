<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View           $this
 * @var app\models\Setting     $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="setting-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>

            <?= $form->field($model, 'setting_system_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'setting_value')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'setting_type')->dropDownList(['checkbox' => 'Checkbox', 'text' => 'Text', 'string' => 'String',], ['prompt' => '']) ?>
            <?= $form->field($model, 'setting_name')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?=
        \yii\bootstrap\Tabs::widget(
            [
                'encodeLabels' => false,
                'items'        => [[
                    'label'   => 'Setting',
                    'content' => $this->blocks['main'],
                    'active'  => true,
                ],]
            ]
        );
        ?>
        <hr/>

        <?=
        Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> '
            . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']
        ); ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
