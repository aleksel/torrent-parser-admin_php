<?php

namespace app\components\behaviors;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Class DateTimestampBehavior
 * @package app\components\behaviors
 */
class DateTimestampBehavior extends TimestampBehavior
{
    /**
     * @inheritdoc
     */
    protected function getValue($event)
    {
        if ($this->value instanceof Expression) {
            return $this->value;
        } else {
            return $this->value !== null ? call_user_func($this->value, $event) : date('Y-m-d H:i:s');
        }
    }
}
