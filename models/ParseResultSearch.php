<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ParseResultSearch represents the model behind the search form about ParseResult.
 */
class ParseResultSearch extends Model
{
    public $parse_result_id;
    public $parse_result_task_id;
    public $parse_result_page_changed;
    public $parse_result_raw_page;
    public $parse_result_searched_element;
    public $parse_result_create_time;

    public function rules()
    {
        return [
            [['parse_result_id', 'parse_result_task_id', 'parse_result_page_changed'], 'integer'],
            [['parse_result_raw_page', 'parse_result_searched_element', 'parse_result_create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parse_result_id'               => 'ID',
            'parse_result_task_id'          => 'ID',
            'parse_result_page_changed'     => 'Изменено',
            'parse_result_raw_page'         => 'Сырые данные',
            'parse_result_searched_element' => 'Tag для поиска',
            'parse_result_create_time'      => 'Дата создания',
        ];
    }

    public function search($params)
    {
        $query = ParseResult::find()->with('parseResultTask');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['parse_result_create_time' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'parse_result_id'           => $this->parse_result_id,
            'parse_result_task_id'      => $this->parse_result_task_id,
            'parse_result_page_changed' => $this->parse_result_page_changed,
        ]);

        $query->andFilterWhere(['like', 'parse_result_raw_page', $this->parse_result_raw_page])
            ->andFilterWhere(['like', 'parse_result_create_time', $this->parse_result_create_time])
            ->andFilterWhere(['like', 'parse_result_searched_element', $this->parse_result_searched_element]);

        return $dataProvider;
    }

    public function searchByTask($params, $task_id)
    {
        $query = ParseResult::find()->where(['parse_result_task_id' => $task_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['parse_result_create_time' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'parse_result_id'           => $this->parse_result_id,
            'parse_result_task_id'      => $this->parse_result_task_id,
            'parse_result_page_changed' => $this->parse_result_page_changed,
        ]);

        $query->andFilterWhere(['like', 'parse_result_raw_page', $this->parse_result_raw_page])
            ->andFilterWhere(['like', 'parse_result_create_time', $this->parse_result_create_time])
            ->andFilterWhere(['like', 'parse_result_searched_element', $this->parse_result_searched_element]);

        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $value = '%' . strtr($value, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
