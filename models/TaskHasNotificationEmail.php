<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_has_notification_email".
 *
 * @property integer $task_has_notification_email_id
 * @property integer $task_has_notification_email_task_id
 * @property integer $task_has_notification_email_notification_email_id
 *
 * @property Task $taskHasNotificationEmailTask
 * @property NotificationEmail $taskHasNotificationEmailNotificationEmail
 */
class TaskHasNotificationEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_has_notification_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_has_notification_email_task_id', 'task_has_notification_email_notification_email_id'], 'required'],
            [['task_has_notification_email_task_id', 'task_has_notification_email_notification_email_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_has_notification_email_id' => Yii::t('app', 'Task Has Notification Email ID'),
            'task_has_notification_email_task_id' => Yii::t('app', 'Task Has Notification Email Task ID'),
            'task_has_notification_email_notification_email_id' => Yii::t('app', 'Task Has Notification Email Notification Email ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasNotificationEmailTask()
    {
        return $this->hasOne(Task::className(), ['task_id' => 'task_has_notification_email_task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasNotificationEmailNotificationEmail()
    {
        return $this->hasOne(NotificationEmail::className(), ['notification_email_id' => 'task_has_notification_email_notification_email_id']);
    }
}
