<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskCategory;

/**
 * TaskCategorySearch represents the model behind the search form about TaskCategory.
 */
class TaskCategorySearch extends Model
{
    public $task_category_id;
    public $task_category_name;
    public $task_category_active;
    public $task_category_create_time;
    public $task_category_update_time;

    public function rules()
    {
        return [
            [['task_category_id', 'task_category_active'], 'integer'],
            [['task_category_name', 'task_category_create_time', 'task_category_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_category_id'          => 'ID',
            'task_category_name'        => 'Наименование',
            'task_category_active'      => 'Активно',
            'task_category_create_time' => 'Дата создания',
            'task_category_update_time' => 'Дата обновления',
        ];
    }

    public function search($params)
    {
        $query = TaskCategory::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $dataProvider->sort->defaultOrder = ['task_category_create_time' => SORT_DESC, 'task_category_update_time' => SORT_DESC];
        $query->andFilterWhere([
            'task_category_id'          => $this->task_category_id,
            'task_category_active'      => $this->task_category_active,
        ]);

        $query->andFilterWhere(['like', 'task_category_create_time', $this->task_category_create_time])
            ->andFilterWhere(['like', 'task_category_update_time', $this->task_category_update_time])
            ->andFilterWhere(['like', 'task_category_name', $this->task_category_name]);

        return $dataProvider;
    }

    public function searchByTask($params, $task_id)
    {
        $query = TaskCategory::find()
            ->innerJoin(
                'task_has_task_category',
                'task_has_task_category.task_has_task_category_task_category_id = task_category.task_category_id'
            )->innerJoin(
                'task',
                'task.task_id = task_has_task_category.task_has_task_category_task_id'
            )->where(['task.task_id' => $task_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $dataProvider->sort->defaultOrder = ['task_category_create_time' => SORT_DESC, 'task_category_update_time' => SORT_DESC];
        $query->andFilterWhere([
            'task_category_id'          => $this->task_category_id,
            'task_category_active'      => $this->task_category_active,
        ]);

        $query->andFilterWhere(['like', 'task_category_create_time', $this->task_category_create_time])
            ->andFilterWhere(['like', 'task_category_update_time', $this->task_category_update_time])
            ->andFilterWhere(['like', 'task_category_name', $this->task_category_name]);

        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $value = '%' . strtr($value, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
