<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskHasNotificationEmail;

/**
 * TaskHasNotificationEmailSearch represents the model behind the search form about TaskHasNotificationEmail.
 */
class TaskHasNotificationEmailSearch extends Model
{
    public $task_has_notification_email_id;
    public $task_has_notification_email_task_id;
    public $task_has_notification_email_notification_email_id;

    public function rules()
    {
        return [
            [['task_has_notification_email_id', 'task_has_notification_email_task_id', 'task_has_notification_email_notification_email_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_has_notification_email_id'                    => 'Task Has Notification Email ID',
            'task_has_notification_email_task_id'               => 'Task Has Notification Email Task ID',
            'task_has_notification_email_notification_email_id' => 'Task Has Notification Email Notification Email ID',
        ];
    }

    public function search($params)
    {
        $query = TaskHasNotificationEmail::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'task_has_notification_email_id'                    => $this->task_has_notification_email_id,
            'task_has_notification_email_task_id'               => $this->task_has_notification_email_task_id,
            'task_has_notification_email_notification_email_id' => $this->task_has_notification_email_notification_email_id,
        ]);

        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $value = '%' . strtr($value, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
