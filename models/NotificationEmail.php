<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notification_email".
 *
 * @property integer                    $notification_email_id
 * @property string                     $notification_email_email
 * @property string                     $notification_email_create_time
 * @property string                     $notification_email_update_time
 *
 * @property TaskHasNotificationEmail[] $taskHasNotificationEmails
 */
class NotificationEmail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_email';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['notification_email_create_time', 'notification_email_update_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['notification_email_update_time']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_email_email'], 'required'],
            [['notification_email_email'], 'email'],
            [['notification_email_create_time', 'notification_email_update_time'], 'safe'],
            [['notification_email_email'], 'string', 'max' => 255],
            [['notification_email_email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_email_id'          => Yii::t('app', 'Notification Email ID'),
            'notification_email_email'       => Yii::t('app', 'Notification Email Email'),
            'notification_email_create_time' => Yii::t('app', 'Notification Email Create Time'),
            'notification_email_update_time' => Yii::t('app', 'Notification Email Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasNotificationEmails()
    {
        return $this->hasMany(TaskHasNotificationEmail::className(), ['task_has_notification_email_notification_email_id' => 'notification_email_id']);
    }
}
