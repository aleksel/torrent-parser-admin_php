<?php

namespace app\models;

use app\components\behaviors\DateTimestampBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "setting".
 *
 * @property integer $setting_id
 * @property string $setting_name
 * @property string $setting_system_name
 * @property string $setting_value
 * @property string $setting_type
 * @property string $setting_create_time
 * @property string $setting_update_time
 */
class Setting extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => DateTimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['setting_create_time', 'setting_update_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['setting_update_time']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_system_name', 'setting_value'], 'required'],
            [['setting_value', 'setting_type'], 'string'],
            [['setting_create_time', 'setting_update_time'], 'safe'],
            [['setting_name', 'setting_system_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => Yii::t('app', 'Setting ID'),
            'setting_name' => Yii::t('app', 'Setting Name'),
            'setting_system_name' => Yii::t('app', 'Setting System Name'),
            'setting_value' => Yii::t('app', 'Setting Value'),
            'setting_type' => Yii::t('app', 'Setting Type'),
            'setting_create_time' => Yii::t('app', 'Setting Create Time'),
            'setting_update_time' => Yii::t('app', 'Setting Update Time'),
        ];
    }
}
