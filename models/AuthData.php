<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auth_data".
 *
 * @property integer $auth_data_id
 * @property string $auth_data_name
 * @property string $auth_data_url
 * @property string $auth_data_data
 * @property string $auth_data_create_time
 * @property string $auth_data_update_time
 *
 * @property Task[] $tasks
 */
class AuthData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_data_name', 'auth_data_url'], 'required'],
            [['auth_data_create_time', 'auth_data_update_time'], 'safe'],
            [['auth_data_name', 'auth_data_url', 'auth_data_data'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auth_data_id' => Yii::t('app', 'Auth Data ID'),
            'auth_data_name' => Yii::t('app', 'Auth Data Name'),
            'auth_data_url' => Yii::t('app', 'Auth Data Url'),
            'auth_data_data' => Yii::t('app', 'Auth Data Data'),
            'auth_data_create_time' => Yii::t('app', 'Auth Data Create Time'),
            'auth_data_update_time' => Yii::t('app', 'Auth Data Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['task_auth_data_id' => 'auth_data_id']);
    }
}
