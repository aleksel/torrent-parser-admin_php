<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_has_task_category".
 *
 * @property integer $task_has_task_category_id
 * @property integer $task_has_task_category_task_id
 * @property integer $task_has_task_category_task_category_id
 *
 * @property TaskCategory $taskHasTaskCategoryTaskCategory
 * @property Task $taskHasTaskCategoryTask
 */
class TaskHasTaskCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_has_task_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_has_task_category_task_id', 'task_has_task_category_task_category_id'], 'required'],
            [['task_has_task_category_task_id', 'task_has_task_category_task_category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_has_task_category_id' => Yii::t('app', 'Task Has Task Category ID'),
            'task_has_task_category_task_id' => Yii::t('app', 'Task Has Task Category Task ID'),
            'task_has_task_category_task_category_id' => Yii::t('app', 'Task Has Task Category Task Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasTaskCategoryTaskCategory()
    {
        return $this->hasOne(TaskCategory::className(), ['task_category_id' => 'task_has_task_category_task_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasTaskCategoryTask()
    {
        return $this->hasOne(Task::className(), ['task_id' => 'task_has_task_category_task_id']);
    }
}
