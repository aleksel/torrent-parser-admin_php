<?php

namespace app\models;

use app\components\behaviors\DateTimestampBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_category".
 *
 * @property integer $task_category_id
 * @property string $task_category_name
 * @property integer $task_category_active
 * @property string $task_category_create_time
 * @property string $task_category_update_time
 *
 * @property TaskHasTaskCategory[] $taskHasTaskCategories
 */
class TaskCategory extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => DateTimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['task_category_create_time', 'task_category_update_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['task_category_update_time']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_category_name'], 'required'],
            [['task_category_active'], 'integer'],
            [['task_category_create_time', 'task_category_update_time'], 'safe'],
            [['task_category_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_category_id' => Yii::t('app', 'Task Category ID'),
            'task_category_name' => Yii::t('app', 'Task Category Name'),
            'task_category_active' => Yii::t('app', 'Task Category Active'),
            'task_category_create_time' => Yii::t('app', 'Task Category Create Time'),
            'task_category_update_time' => Yii::t('app', 'Task Category Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasTaskCategories()
    {
        return $this->hasMany(TaskHasTaskCategory::className(), ['task_has_task_category_task_category_id' => 'task_category_id']);
    }
}
