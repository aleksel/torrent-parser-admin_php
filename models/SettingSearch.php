<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Setting;

/**
 * SettingSearch represents the model behind the search form about Setting.
 */
class SettingSearch extends Model
{
    public $setting_id;
    public $setting_name;
    public $setting_system_name;
    public $setting_value;
    public $setting_type;
    public $setting_create_time;
    public $setting_update_time;

    public function rules()
    {
        return [
            [['setting_id'], 'integer'],
            [['setting_name', 'setting_system_name', 'setting_value', 'setting_type', 'setting_create_time', 'setting_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id'          => 'ID',
            'setting_name'        => 'Наименование',
            'setting_system_name' => 'Системное наименование',
            'setting_value'       => 'Значение',
            'setting_type'        => 'Тип',
            'setting_create_time' => 'Дата создания',
            'setting_update_time' => 'Дата обновления',
        ];
    }

    public function search($params)
    {
        $query = Setting::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['setting_update_time' => SORT_DESC, 'setting_create_time' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'setting_id' => $this->setting_id,
        ]);

        $query->andFilterWhere(['like', 'setting_name', $this->setting_name])
            ->andFilterWhere(['like', 'setting_system_name', $this->setting_system_name])
            ->andFilterWhere(['like', 'setting_value', $this->setting_value])
            ->andFilterWhere(['like', 'setting_create_time', $this->setting_create_time])
            ->andFilterWhere(['like', 'setting_update_time', $this->setting_update_time])
            ->andFilterWhere(['like', 'setting_type', $this->setting_type]);

        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $value = '%' . strtr($value, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
