<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskHasTaskCategory;

/**
 * TaskHasTaskCategorySearch represents the model behind the search form about TaskHasTaskCategory.
 */
class TaskHasTaskCategorySearch extends Model
{
	public $task_has_task_category_id;
	public $task_has_task_category_task_id;
	public $task_has_task_category_task_category_id;

	public function rules()
	{
		return [
			[['task_has_task_category_id', 'task_has_task_category_task_id', 'task_has_task_category_task_category_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'task_has_task_category_id' => 'ID',
			'task_has_task_category_task_id' => 'Task ID',
			'task_has_task_category_task_category_id' => 'Category ID',
		];
	}

	public function search($params)
	{
		$query = TaskHasTaskCategory::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'task_has_task_category_id' => $this->task_has_task_category_id,
            'task_has_task_category_task_id' => $this->task_has_task_category_task_id,
            'task_has_task_category_task_category_id' => $this->task_has_task_category_task_category_id,
        ]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
