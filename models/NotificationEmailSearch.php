<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationEmail;

/**
 * NotificationEmailSearch represents the model behind the search form about NotificationEmail.
 */
class NotificationEmailSearch extends Model
{
    public $notification_email_id;
    public $notification_email_email;
    public $notification_email_create_time;
    public $notification_email_update_time;

    public function rules()
    {
        return [
            [['notification_email_id'], 'integer'],
            [['notification_email_email', 'notification_email_create_time', 'notification_email_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_email_id'          => 'Notification Email ID',
            'notification_email_email'       => 'Notification Email Email',
            'notification_email_create_time' => 'Notification Email Create Time',
            'notification_email_update_time' => 'Notification Email Update Time',
        ];
    }

    public function search($params)
    {
        $query = NotificationEmail::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $dataProvider->sort->defaultOrder = ['notification_email_update_time' => SORT_DESC, 'notification_email_create_time' => SORT_DESC];
        $query->andFilterWhere([
            'notification_email_id'          => $this->notification_email_id,
        ]);

        $query->andFilterWhere(['like', 'notification_email_email', $this->notification_email_email])
            ->andFilterWhere(['like', 'notification_email_create_time', $this->notification_email_create_time])
            ->andFilterWhere(['like', 'notification_email_update_time', $this->notification_email_update_time]);

        return $dataProvider;
    }

    public function searchByTask($params, $task_id)
    {
        $query = NotificationEmail::find()
            ->innerJoin(
                'task_has_notification_email',
                'task_has_notification_email.task_has_notification_email_notification_email_id = notification_email.notification_email_id'
            )->innerJoin(
                'task',
                'task.task_id = task_has_notification_email.task_has_notification_email_task_id'
            )->where(['task.task_id' => $task_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $dataProvider->sort->defaultOrder = ['notification_email_update_time' => SORT_DESC, 'notification_email_create_time' => SORT_DESC];
        $query->andFilterWhere([
            'notification_email_id'          => $this->notification_email_id,
        ]);

        $query->andFilterWhere(['like', 'notification_email_email', $this->notification_email_email])
            ->andFilterWhere(['like', 'notification_email_create_time', $this->notification_email_create_time])
            ->andFilterWhere(['like', 'notification_email_update_time', $this->notification_email_update_time]);

        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $value = '%' . strtr($value, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
