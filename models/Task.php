<?php

namespace app\models;

use app\components\behaviors\DateTimestampBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property integer $task_id
 * @property integer $task_auth_data_id
 * @property string $task_url
 * @property string $task_name
 * @property string $task_search_tag
 * @property string $task_comparison_element
 * @property integer $task_active
 * @property string $task_create_time
 * @property string $task_update_time
 *
 * @property ParseResult[] $parseResults
 * @property AuthData $taskAuthData
 * @property TaskHasNotificationEmail[] $taskHasNotificationEmails
 * @property TaskHasTaskCategory[] $taskHasTaskCategories
 */
class Task extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => DateTimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['task_create_time', 'task_update_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['task_update_time']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_auth_data_id', 'task_active'], 'integer'],
            [['task_url', 'task_name', 'task_comparison_element'], 'required'],
            [['task_comparison_element'], 'string'],
            [['task_create_time', 'task_update_time'], 'safe'],
            [['task_url', 'task_name', 'task_search_tag'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task ID'),
            'task_auth_data_id' => Yii::t('app', 'Task Auth Data ID'),
            'task_url' => Yii::t('app', 'Task Url'),
            'task_name' => Yii::t('app', 'Task Name'),
            'task_search_tag' => Yii::t('app', 'Task Search Tag'),
            'task_comparison_element' => Yii::t('app', 'Task Comparison Element'),
            'task_active' => Yii::t('app', 'Task Active'),
            'task_create_time' => Yii::t('app', 'Task Create Time'),
            'task_update_time' => Yii::t('app', 'Task Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParseResults()
    {
        return $this->hasMany(ParseResult::className(), ['parse_result_task_id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskAuthData()
    {
        return $this->hasOne(AuthData::className(), ['auth_data_id' => 'task_auth_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasNotificationEmails()
    {
        return $this->hasMany(TaskHasNotificationEmail::className(), ['task_has_notification_email_task_id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHasTaskCategories()
    {
        return $this->hasMany(TaskHasTaskCategory::className(), ['task_has_task_category_task_id' => 'task_id']);
    }
}
