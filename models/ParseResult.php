<?php

namespace app\models;

use app\components\behaviors\DateTimestampBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "parse_result".
 *
 * @property integer $parse_result_id
 * @property integer $parse_result_task_id
 * @property integer $parse_result_page_changed
 * @property string  $parse_result_raw_page
 * @property string  $parse_result_searched_element
 * @property string  $parse_result_create_time
 *
 * @property Task    $parseResultTask
 */
class ParseResult extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'      => DateTimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['parse_result_create_time']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parse_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parse_result_task_id', 'parse_result_page_changed', 'parse_result_raw_page', 'parse_result_searched_element'], 'required'],
            [['parse_result_task_id', 'parse_result_page_changed'], 'integer'],
            [['parse_result_raw_page'], 'string'],
            [['parse_result_create_time'], 'safe'],
            [['parse_result_searched_element'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parse_result_id'               => Yii::t('app', 'Parse Result ID'),
            'parse_result_task_id'          => Yii::t('app', 'Parse Result Task ID'),
            'parse_result_page_changed'     => Yii::t('app', 'Parse Result Page Changed'),
            'parse_result_raw_page'         => Yii::t('app', 'Parse Result Raw Page'),
            'parse_result_searched_element' => Yii::t('app', 'Parse Result Searched Element'),
            'parse_result_create_time'      => Yii::t('app', 'Parse Result Create Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParseResultTask()
    {
        return $this->hasOne(Task::className(), ['task_id' => 'parse_result_task_id']);
    }
}
