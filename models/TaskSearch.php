<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about Task.
 */
class TaskSearch extends Model
{
    public $task_id;
    public $category;
    public $task_auth_data_id;
    public $task_url;
    public $task_name;
    public $task_search_tag;
    public $task_comparison_element;
    public $task_active;
    public $task_create_time;
    public $task_update_time;

    public function rules()
    {
        return [
            [['task_id', 'task_auth_data_id', 'task_active'], 'integer'],
            [['category', 'task_url', 'task_name', 'task_search_tag', 'task_comparison_element', 'task_create_time', 'task_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id'                 => 'Task ID',
            'task_auth_data_id'       => 'Task Auth Data ID',
            'task_url'                => 'Task Url',
            'task_name'               => 'Task Name',
            'task_search_tag'         => 'Task Search Tag',
            'task_comparison_element' => 'Task Comparison Element',
            'task_active'             => 'Task Active',
            'task_create_time'        => 'Task Create Time',
            'task_update_time'        => 'Task Update Time',
        ];
    }

    public function search($params)
    {
        $query = Task::find();
        $query->with([
            'taskHasTaskCategories' => function ($query) {
                $query->with([
                    'taskHasTaskCategoryTaskCategory' /*=> function ($query) {
                        $query->andWhere(['type' => AuthItem::TYPE_PERMISSION]);
                    }*/
                ]);
            },
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['task_update_time' => SORT_DESC, 'task_create_time' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        if ($this->category) {
            $query->innerJoin('task_has_task_category', 'task.task_id = task_has_task_category.task_has_task_category_task_id');
            $query->innerJoin('task_category', $this->category . ' = task_has_task_category.task_has_task_category_task_category_id');
        }
        $query->andFilterWhere([
            'task_id'           => $this->task_id,
            'task_auth_data_id' => $this->task_auth_data_id,
            'task_active'       => $this->task_active,
        ]);

        $query->andFilterWhere(['like', 'task_url', $this->task_url])
            ->andFilterWhere(['like', 'task_name', $this->task_name])
            ->andFilterWhere(['like', 'task_search_tag', $this->task_search_tag])
            ->andFilterWhere(['like', 'task_create_time', $this->task_create_time])
            ->andFilterWhere(['like', 'task_update_time', $this->task_update_time])
            ->andFilterWhere(['like', 'task_comparison_element', $this->task_comparison_element]);

        return $dataProvider;
    }

    public function searchByEmail($params, $notification_email_id)
    {
        $query = Task::find()
            ->innerJoin(
                'task_has_notification_email',
                'task_has_notification_email.task_has_notification_email_task_id = task.task_id'
            )->innerJoin(
                'notification_email',
                'notification_email.notification_email_id = task_has_notification_email.task_has_notification_email_notification_email_id'
            )->where(['notification_email.notification_email_id' => $notification_email_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['task_update_time' => SORT_DESC, 'task_create_time' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'task_id'           => $this->task_id,
            'task_auth_data_id' => $this->task_auth_data_id,
            'task_active'       => $this->task_active,
        ]);

        $query->andFilterWhere(['like', 'task_url', $this->task_url])
            ->andFilterWhere(['like', 'task_name', $this->task_name])
            ->andFilterWhere(['like', 'task_search_tag', $this->task_search_tag])
            ->andFilterWhere(['like', 'task_create_time', $this->task_create_time])
            ->andFilterWhere(['like', 'task_update_time', $this->task_update_time])
            ->andFilterWhere(['like', 'task_comparison_element', $this->task_comparison_element]);

        return $dataProvider;
    }

    public function searchByTaskCategory($params, $task_category_id)
    {
        $query = Task::find()
            ->innerJoin(
                'task_has_task_category',
                'task_has_task_category.task_has_task_category_task_id = task.task_id'
            )->innerJoin(
                'task_category',
                'task_category.task_category_id = task_has_task_category.task_has_task_category_task_category_id'
            )->where(['task_category.task_category_id' => $task_category_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['task_update_time' => SORT_DESC, 'task_create_time' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'task_id'           => $this->task_id,
            'task_auth_data_id' => $this->task_auth_data_id,
            'task_active'       => $this->task_active,
        ]);

        $query->andFilterWhere(['like', 'task_url', $this->task_url])
            ->andFilterWhere(['like', 'task_name', $this->task_name])
            ->andFilterWhere(['like', 'task_search_tag', $this->task_search_tag])
            ->andFilterWhere(['like', 'task_create_time', $this->task_create_time])
            ->andFilterWhere(['like', 'task_update_time', $this->task_update_time])
            ->andFilterWhere(['like', 'task_comparison_element', $this->task_comparison_element]);

        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $value = '%' . strtr($value, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
